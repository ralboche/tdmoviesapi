module.exports = (sequelize, DataTypes) => {
  const Producer = sequelize.define("producer", {
    firstname: {
      type: DataTypes.STRING
    },
    lastname: {
      type: DataTypes.STRING
    },
    nationality: {
      type: DataTypes.STRING
      },
    birthdate: {
      type: DataTypes.STRING
    }
  });

  return Producer;
};



