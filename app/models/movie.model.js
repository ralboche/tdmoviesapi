module.exports = (sequelize, DataTypes) => {
  const Movie = sequelize.define("movie", {
    title: {
      type: DataTypes.STRING,
    },
    year: {
      type: DataTypes.STRING,
    },
    language: {
      type: DataTypes.STRING,
    },
    type: {
      type: DataTypes.STRING,
    },
    poster: {
      type: DataTypes.STRING,
    },
    synopsis: {
      type: DataTypes.STRING,
    },
    rate: {
      type: DataTypes.INTEGER,
    },
  });

  return Movie;
};
