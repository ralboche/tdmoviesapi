const db = require("../models");
const Producer = db.producers;

// Create and Save a new Producer
exports.create = (req, res) => {
  // Validate request
  if (!req.body.lastname) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
    return;
  }

  // Create a Producer
  const producer = {
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    nationality: req.body.nationality,
    birthdate: req.body.birthdate,
  };

  // Save Movie in the database
  Producer.create(producer)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Movie.",
      });
    });
};

// Retrieve all Producers from the database.
exports.findAll = (req, res) => {
  Producer.findAll()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving movies.",
      });
    });
};

// Find a single Producer with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Producer.findByPk(id)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error retrieving Movie with id=" + id,
      });
    });
};
