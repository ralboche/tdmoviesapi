const db = require("../models");
const Movie = db.movies;
const Op = db.Sequelize.Op;

// Create and Save a new Movie
exports.create = (req, res) => {
  if (
    !req.body.title ||
    !req.body.year ||
    !req.body.language ||
    !req.body.type ||
    !req.body.poster ||
    !req.body.producerId ||
    !req.body.synopsis
  ) {
    res.status(400).send({
      message: "All fields are requireds",
    });
    return;
  }

  // Create a Movie
  const movie = {
    title: req.body.title,
    year: req.body.year,
    language: req.body.language,
    type: req.body.type,
    poster: req.body.poster,
    synopsis: req.body.synopsis,
    producerId: req.body.producerId,
  };

  // Save Movie in the database
  Movie.create(movie)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Movie.",
      });
    });
};

// Retrieve all Movies from the database.
exports.findAll = (req, res) => {
  const search = req.query.search;
  var condition = search
    ? {
        [Op.or]: [
          { title: { [Op.like]: `%${search}%` } },
          { year: { [Op.like]: `%${search}%` } },
        ],
      }
    : null;
  Movie.findAll({ include: ["producer"], where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving movies.",
      });
    });
};

// Find a single Movie with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Movie.findByPk(id, { include: ["producer"] })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error retrieving Movie with id=" + id,
      });
    });
};

// Update a Movie by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Movie.update(req.body, {
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Movie was updated successfully.",
        });
      } else {
        res.send({
          message: `Cannot update Movie with id=${id}. Maybe Movie was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating Movie with id=" + id,
      });
    });
};

// Delete a Movie with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Movie.destroy({
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Movie was deleted successfully!",
        });
      } else {
        res.send({
          message: `Cannot delete Movie with id=${id}. Maybe Movie was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete Movie with id=" + id,
      });
    });
};
