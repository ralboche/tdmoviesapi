module.exports = (app) => {
  const producers = require("../controllers/producer.controller.js");

  var router = require("express").Router();

  // Create a new Producer
  router.post("/", producers.create);

  // Retrieve all Producers
  router.get("/", producers.findAll);

  // Retrieve a single Movie with id
  router.get("/:id", producers.findOne);

  app.use("/api/producers", router);
};
